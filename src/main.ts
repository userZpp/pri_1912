import { createApp } from "vue"
import App from "./App.vue"
import { createPinia } from "pinia"
import router from "./router"
import "@/styles/common.less"
import uiPlugin from "@/plugin/ui.registry"
import "@/styles/theme.css"
import "@/styles/var.less"
import Header from "@/plugin/baseComponent.registry"

createApp(App)
  .use(createPinia())
  .use(router)
  .use(uiPlugin)
  .use(Header)
  .mount("#app")
