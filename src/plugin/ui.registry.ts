import { App } from "vue"
import { Button, message } from "ant-design-vue"
const install = (app: App) => {
  app.use(Button)
  app.config.globalProperties.$message = message
}
export default {
  install,
}
