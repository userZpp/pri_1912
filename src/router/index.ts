import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
// import "./baseRouter"
const routes: Array<RouteRecordRaw> = [
  {
    path: "/home",
    name: "home",
    component: () => import("@/views/home/Home.vue"),
    children: [
      {
        path: "/home/article",
        name: "Article",
        component: () => import("@/views/home/article/Article.vue"),
      },
      {
        path: "/home/archive",
        name: "Archive",
        component: () => import("@/views/home/archive/Archive.vue"),
      },
      {
        path: "/home/kbrochure",
        name: "Kbrochure",
        component: () => import("@/views/home/kbrochure/Kbrochure.vue"),
      },
      {
        path: "/home",
        redirect: "/home/article",
      },
    ],
  },
  {
    path: "/edit",
    name: "Edit",
    component: () => import("@/views/edit/Edit.vue"),
  },
  {
    path: "/page/:id",
    component: () => import("@/views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/",
    redirect: "/home",
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
