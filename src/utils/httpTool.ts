import axios from "axios"
import { message } from "ant-design-vue"
import CODEMAP from "./coodMap"
export interface PageNationParams {
  page?: number
  pageSize?: number
}

export interface Params extends PageNationParams {
  [propertyName: string]: string | number | boolean | undefined
}

const httpTool = axios.create({
  timeout: 10000,
  baseURL: process.env.VUE_APP_BASEURL,
})

httpTool.interceptors.response.use(
  (response) => {
    if (response.data?.success) {
      return response.data
    }
    message.error(
      response.data?.msg || CODEMAP[response.data?.statusCode || 500]
    )
    return response
  },
  (error) => {
    if (error.code === "ECONNABORTED") {
      // 网络超时
      message.error("您当前网络环境不好，请刷新重试~")
    } else {
      message.error(
        error.response?.statusText ||
          CODEMAP[error.response.data?.status || 400]
      )
    }
    return Promise.reject(error)
  }
)
export default {
  ...httpTool,
  get(url: string, params: Params) {
    return httpTool.get(url, {
      params,
    })
  },
  post() {
    console.log("post")
  },
}
