import { defineStore } from "pinia"
import { base } from "@/services"
const useStore = defineStore("baseStore", {
  state: () => ({
    navList: [] as any,
    list: [] as any
  }),
  actions: {
    async getNavData() {
      const { data } = await base.getNavPage()
      this.$patch({
        navList: data[0].filter((item: any) => item.id),
      })
    },
    async getDataList() {
      const { data } = await base.getList({});
      this.$patch({
        list: data[0]
      })
      console.log(data[0],'555');
      
    }
  },
})

export default useStore
