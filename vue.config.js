const { defineConfig } = require("@vue/cli-service")
const path = require("path")
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"]
    types.forEach((type) =>
      addStyleResource(config.module.rule("less").oneOf(type))
    )
    config.plugin("html").tap((args) => {
      args[0].title = "八维创作平台"
      return args
    })
  },
  devServer: {
    proxy: {
      // 开发环境的时候 解决跨域问题
      "/api": {
        target: "https://creationapi.shbwyz.com",
        ws: true,
        changeOrigin: true,
      },
    },
  },
})
function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/styles/var.less")],
    })
}
